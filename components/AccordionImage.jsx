import { Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, Image, Text } from "@chakra-ui/react";

export default function AccordionImage({ src }) {
    return (
        <Accordion allowMultiple>
            <AccordionItem>
                <AccordionButton>
                    <Text>Mostrar imagem</Text>
                    <AccordionIcon />
                </AccordionButton>
                <AccordionPanel>
                    <Image w={'2xl'} src={src} alt={'imagem de tutorial'} />
                </AccordionPanel>
            </AccordionItem>
        </Accordion>
    )
}