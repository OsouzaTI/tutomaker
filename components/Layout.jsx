import { Box, Grid, GridItem } from "@chakra-ui/react";
import { isMobileScreen } from "../utils/responsive";
import Sidebar from "./Sidebar";

export default function Layout({children}) {

    let isMobile = isMobileScreen();

    function responsive() {

        const normal = {
            templateAreas: ` "side app" `,
            gridTemplateRows: '1fr',
            gridTemplateColumns: '.2fr 1fr',
            h: '100vh'
        };

        const mobile = {
            templateAreas: `
                "side"
                "app"
            `,
            gridTemplateRows: '.1fr 1fr',
            gridTemplateColumns: '1fr',
            w: '100vw',
            h: '100vh'
        };

        return isMobile ? mobile : normal;

    }

    function responsiveSideBar() {
        return isMobile 
            ? {} 
            : { 
                h: '100vh',
                overflowY: 'auto',
                borderRight: '.1rem solid gray'
            };
    }

    const gridResponsiveData = responsive();

    return (
        <Grid 
            {...gridResponsiveData}
        >
            <GridItem area={'side'} {...responsiveSideBar()}>
                <Sidebar />
            </GridItem>
            <GridItem area={'app'}>{children}</GridItem>            
        </Grid>
    );

}