import { ChevronRightIcon, HamburgerIcon, TriangleDownIcon } from "@chakra-ui/icons";
import { Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, Box, Button, Drawer, DrawerBody, DrawerCloseButton, DrawerContent, DrawerFooter, DrawerHeader, DrawerOverlay, HStack, IconButton, Link, Text, useDisclosure, useMediaQuery, VStack } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useRef } from "react";
import tutoriais from "../database/tutoriais";
import { isMobileScreen } from "../utils/responsive";

function DrawerSidebar({onClick}) {

    const { isOpen, onOpen, onClose } = useDisclosure()
    const btnRef = useRef()
  
    return (
      <>
        <Box w={'full'} position={'fixed'} top={0} bg={'gray.50'} shadow={'md'}>
            <IconButton 
                rounded={'none'} 
                ref={btnRef} 
                onClick={onOpen} 
                variant={'ghost'}
                icon={<HamburgerIcon />} />
        </Box>
        <Drawer
          isOpen={isOpen}
          placement={'left'}
          onClose={onClose}
          finalFocusRef={btnRef}
        >
            <DrawerOverlay />
            <DrawerContent>
                <DrawerCloseButton />
                <DrawerHeader>Tutoriais</DrawerHeader>

                <DrawerBody>
                    <NormalSidebar onClose={onClose} onClick={onClick} />
                </DrawerBody>

                <DrawerFooter></DrawerFooter>
            </DrawerContent>
        </Drawer>
    </>
    )
}

function NormalSidebar({onClose = function(){}, onClick}) {

    return (
        <VStack>
            <Accordion w={'full'} >
            {tutoriais.map((tutorial, i)=>(
                    <AccordionItem key={i}>
                        <AccordionButton onClick={()=>onClick(i)} _hover={{bg:'blue.600'}} bg={'blue.500'} color={'white'} fontWeight={'bold'}>
                            <Text w={'full'} textAlign={'left'}>{tutorial.title}</Text>
                            <AccordionIcon />
                        </AccordionButton>
                        <AccordionPanel>
                            <VStack pl={4} textAlign={'left'}>
                                {tutorial.passos.map(({subtitle, description}, j)=>(
                                    <Link key={j} w={'full'} onClick={onClose} href={`#passo_${j}`}>
                                        <Text _hover={{color:'blue.600'}} cursor={'pointer'} textAlign={'left'} wordBreak={'normal'}>{`# ${subtitle}`}</Text>
                                    </Link>
                                ))}
                            </VStack>
                        </AccordionPanel>
                    </AccordionItem>
            ))}
            </Accordion>
        </VStack>
    );

}

export default function Sidebar() {

    const isMobile = isMobileScreen();
    const router = useRouter();
    function onClick(tutorialId) {
        router.push(`/tutorial/${tutorialId}`);
    }

    return (
        isMobile
            ? <DrawerSidebar onClick={onClick} /> 
            : <NormalSidebar onClick={onClick} />
    );

}