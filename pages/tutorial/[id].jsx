import { ChevronRightIcon } from "@chakra-ui/icons";
import { Box, Flex, Heading, Image, Text, VStack } from "@chakra-ui/react";
import AccordionImage from "../../components/AccordionImage";
import tutoriais from "../../database/tutoriais";
import { isMobileScreen } from "../../utils/responsive";

export default function id(props) {

    const isMobile = isMobileScreen();
    const { id, passos } = props;

    function responsive() {
        
        const normal = {
            h: '100vh',
            overflowY: 'auto'
        };

        const mobile = {};
        return isMobile ? mobile : normal;
    }

    return (
        <VStack alignItems={'center'} py={10} gap={4} {...responsive()} >
            {passos.map(({subtitle, description, subpassos}, i)=>{
                return (
                    <Box key={i} w={isMobile ? 'full' : '60%'} id={`passo_${i}`} _focus={{bg: '#ff0'}}>
                        {subtitle 
                            ? <Heading py={10} fontSize={'lg'}><ChevronRightIcon fontSize={'xl'} /> {subtitle}</Heading>
                            : <></>
                        }
                        <Flex flexDir={'column'} ml={10} gap={4}>
                            <Text dangerouslySetInnerHTML={{__html: description}} />
                            {subpassos.map(({image, description}, i)=>{
                                return (
                                    <VStack alignItems={'start'} key={i} gap={4}>
                                        {image ? <AccordionImage src={image}/> : <></>}
                                        <Text dangerouslySetInnerHTML={{__html: description}} />
                                    </VStack>
                                )
                            })}
                        </Flex>
                    </Box>
                )                           
            })}
        </VStack>
    );

}

export async function getServerSideProps(context) {

    const { id } = context.query;
    const { passos } = tutoriais[id];

    return {
        props: {id, passos}, // will be passed to the page component as props
    }
}
  