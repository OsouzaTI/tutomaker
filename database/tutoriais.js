const tutoriais = [
    {
        title: 'Verificação de erros no HD e uso do desfragmentador no PC',
        date: new Date(),
        passos: [
            {   
                subtitle: 'Desfragmentar e Otimizar unidades',
                description: `
                    Abra o menu iniciar e digite <b>desfragmentador de disco</b>
                `,
                subpassos: [
                    {
                        image: '/images/desfragmentador/digite-desfrag.png',
                        description: `
                            Na lista de resultados clique em <b>Desfragmentar e Otimizar unidades</b>
                        `
                    },
                ]
            },
            {
                subtitle: 'Otimizador de unidades',
                description: '',
                subpassos: [
                    {
                        image: '/images/desfragmentador/otimizar-unidades.png',
                        description: `
                            Na janela do Otimizador de unidades, escolha uma unidade que deseja ser desfragmentada
                            e clique em <b>analisar</b>, esse processo de analise pode levar alguns minutos ou talvez horas
                            dependendo do quanto está fragmentada a unidade escolhida.
                            <br><br>
                            Apos a analise, caso esteja fragmentada
                            clique em <b>Otimizar</b> (assim como na analise isso podera levar algum tempo).
                            <br><br>
                            Caso seja solicitada a senha de administrador durante o processo, digite-a e prossiga.
                        `
                    },
                ],
            },
        ]
    },
    {
        title: 'Parear PC e smartphone (conectar via bluetooth)',
        date: new Date(),
        passos: []
    },
    {
        title: 'Restaurar PC',
        date: new Date(),
        passos: []
    },
    {
        title: 'Finalizar processos (forçar parada) de um programa mobile',
        date: new Date(),
        passos: []
    },
    {
        title: 'Instalar uma impressora no PC, via USB',
        date: new Date(),
        passos: []
    }
];


export default tutoriais;