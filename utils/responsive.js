import { useMediaQuery } from "@chakra-ui/react";

function isMobileScreen() {
    const [isMobileScreen] = useMediaQuery('(max-width: 40em)');
    return isMobileScreen;
}

export {
    isMobileScreen
}